from django.conf.urls import url
from rides import views

urlpatterns = [
    url(r'^ride/$', views.create_ride),
    url(r'^ride/(?P<id>[0-9]+)/$', views.ride_detail),
    url(r'^ride/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.ride_list),
    url(r'^ride/creator/(?P<id>[0-9]+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.ride_creator),
    url(r'^ride/destination/(?P<id>[0-9]+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.ride_destination),
    url(r'^ride/source/(?P<id1>[0-9]+)/destination/(?P<id2>[0-9]+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.ride_creator),

    url(r'^ride/log/$', views.ride_log_list),
    url(r'^ride/log/(?P<id>[0-9]+)/$', views.ride_log_detail),
    url(r'^ride/log/passenger/(?P<id>[0-9]+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.ride_log_passenger),
    url(r'^ride/log/ride/(?P<id>[0-9]+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.ride_log_ride),

    url(r'^ride/apply/passenger/$', views.ride_passenger_list),
    url(r'^ride/apply/passenger/(?P<id>[0-9]+)/$', views.ride_passenger_detail),
    url(r'^ride/apply/passenger/(?P<id>[0-9]+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.ride_apply_passenger),
    url(r'^ride/apply/passenger/ride/(?P<id>[0-9]+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.ride_apply_passenger_ride),

    url(r'^ride/apply/driver/$', views.ride_driver_list),
    url(r'^ride/apply/driver/(?P<id>[0-9]+)/$', views.ride_driver_detail),
    url(r'^ride/apply/driver/(?P<id>[0-9]+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.ride_apply_driver),
    url(r'^ride/apply/driver/ride/(?P<id>[0-9]+)/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.ride_apply_driver_ride),

    url(r'^ride/invitation/$', views.invitation_list),
    url(r'^ride/invitation/(?P<id>[0-9]+)/$', views.invitation_detail),
    url(r'^ride/invitation/ride/(?P<id>[0-9]+)/$', views.ride_invitation_ride),
]