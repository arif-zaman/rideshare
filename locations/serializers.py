from models import Location, University, Community
from rest_framework import serializers


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ('id', 'name', 'created', 'updated')


class UniversitySerializer(serializers.ModelSerializer):
    class Meta:
        model 	= University
        fields 	= ('id', 'name', 'email_ext', 'created', 'updated')


class UpdateUniversitySerializer(serializers.Serializer):
    name 		= serializers.CharField(required=False)
    email_ext 	= serializers.CharField(required=False)


class CommunitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Community
        fields = ('id', 'name', 'university', 'created','updated')


class UpdateCommunitySerializer(serializers.Serializer):
    name        = serializers.CharField(required=False)
    university  = serializers.IntegerField(required=False)


