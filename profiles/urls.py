from django.conf.urls import url
from profiles import views

urlpatterns = [
    url(r'^user/login/$', views.login),
    url(r'^user/change/password/$', views.change_password),
    url(r'^admin/$', views.create_admin),
    url(r'^admin/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.admin_list),
    url(r'^admin/(?P<id>[0-9]+)/$', views.admin_detail),
    url(r'^passenger/$', views.create_passenger),
    url(r'^passenger/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.passenger_list),
    url(r'^passenger/(?P<id>[0-9]+)/$', views.passenger_detail),
    url(r'^company/$', views.create_company),
    url(r'^company/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.company_list),
    url(r'^company/(?P<id>[0-9]+)/$', views.company_detail),
    url(r'^driver/$', views.create_driver),
    url(r'^driver/page/(?P<pageName>\d+)/size/(?P<pageSize>\d+)/$', views.driver_list),
    url(r'^driver/(?P<id>[0-9]+)/$', views.driver_detail),
    url(r'^taxi/$', views.taxi_list),
    url(r'^taxi/(?P<id>[0-9]+)/$', views.taxi_detail),
]