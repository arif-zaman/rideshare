from django.db import models
from django.contrib.auth.models import User
from locations.models import University, Community


class Admin(models.Model):
    name        = models.CharField(max_length=60)
    role        = models.CharField(max_length=60)
    email       = models.EmailField(unique=True, db_index=True)
    password    = models.CharField(max_length=100)
    propic      = models.FileField(upload_to='ProfilePhoto/%Y/%m/%d',null=True,blank=True)
    info        = models.CharField(max_length=1200, blank=True, default="")
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated     = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        ordering = ['-created']


class Passenger(models.Model):
    name        = models.CharField(max_length=60)
    role        = models.CharField(max_length=60)
    email       = models.EmailField(unique=True, db_index=True)
    password    = models.CharField(max_length=100)
    university  = models.ForeignKey(University, on_delete=models.CASCADE)
    community   = models.ForeignKey(Community, on_delete=models.CASCADE)
    propic      = models.FileField(upload_to='ProfilePhoto/%Y/%m/%d',null=True,blank=True)
    info        = models.CharField(max_length=1200, blank=True, default="")
    created     = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated     = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        ordering = ['university', 'community', '-created']


class Company(models.Model):
    name    = models.CharField(max_length=60)
    role    = models.CharField(max_length=60)
    email   = models.EmailField(unique=True, db_index=True)
    password    = models.CharField(max_length=100)
    license = models.CharField(max_length=30,unique=True)
    propic  = models.FileField(upload_to='ProfilePhoto/%Y/%m/%d',null=True,blank=True)
    info    = models.CharField(max_length=1200,blank=True,null=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        ordering = ['-created']


class Driver(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, default=1)
    name    = models.CharField(max_length=60)
    role    = models.CharField(max_length=60)
    email   = models.EmailField(unique=True, db_index=True)
    password    = models.CharField(max_length=100)
    license = models.CharField(max_length=30,unique=True)
    propic  = models.FileField(upload_to='ProfilePhoto/%Y/%m/%d',null=True,blank=True)
    info    = models.CharField(max_length=1200,blank=True,null=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        ordering = ['-created']


class Taxi(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, db_index=True)
    driver  = models.OneToOneField(Driver, on_delete=models.CASCADE, null=True)
    license = models.CharField(max_length=30,unique=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    class Meta:
        ordering = ['company', '-created']